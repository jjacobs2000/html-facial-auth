'use strict';

angular.module('facialUiApp')
  .service('Namefactory', function Namefactory() {
		return {
			firstname : '',
			lastname : '',
			name : ''
		}
  });
